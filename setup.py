import os
import sys

from codecs import open
from setuptools import setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass into pytest")]

    def initialize_options(self):
        super().initialize_options()
        self.pytest_args = '-n auto'

    def run_tests(self):
        import shlex
        import pytest
        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)


install_requires = [
    'requests >= 2.20',
    'schematics >= 2.1.0',
]

tests_require = [
    'pytest >= 3.4',
    'pytest-mock >= 1.7',
    'pytest-xdist >= 1.22',
    'pytest-cov >= 2.5',
]

here = os.path.abspath(os.path.dirname(__file__))
os.chdir(here)

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

about = {}
module = os.path.join(here, 'tesuto', 'core', '__version__.py')
with open(module, 'r', encoding='utf-8') as f:
    exec(f.read(), about)

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    license=about['__license__'],
    keywords='tesuto api',
    packages=['tesuto.apis', 'tesuto.core', 'tesuto.models', 'tesuto.resources', 'tesuto.tesuto'],
    zip_safe=False,
    install_requires=install_requires,
    python_requires=">=3.4",
    tests_require=tests_require,
    cmdclass={'test': PyTest},
    project_urls={
        'Bug Tracker': 'https://gitlab.com/tesuto/public/tesuto-python/issues',
        'Documentation': 'https://api-docs.tesuto.com',
        'Source Code': 'https://gitlab.com/tesuto/public/tesuto-python'
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    scripts=[
        'bin/tesuto'
    ],
)
