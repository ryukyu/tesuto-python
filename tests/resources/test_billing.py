from tesuto.apis import Billing


class TestVendor(object):
    def test_list(self):
        resp = Billing.list(missing=True)
        assert resp.status_code in [200, 403]
        assert isinstance(resp.data, list)
