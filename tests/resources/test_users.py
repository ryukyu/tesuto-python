from tesuto.apis import User


class TestUser(object):
    def test_list(self):
        resp = User.list(missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, list)
        assert isinstance(resp.data[0], User)
        assert isinstance(resp.data[0].id, int)

    def test_user(self):
        resp = User.list(missing=True)
        test_user = resp.data[0]
        resp = User.put(test_user.id, data=dict(name="tesuto admin"))
        assert resp.data.name == 'tesuto admin'
        resp = test_user.update(data=dict(name=test_user.name))
        assert resp.status_code == 200
        resp_user = User.get(test_user.id).data
        assert resp_user.name == test_user.name
