from tesuto.apis import Vendor


class TestVendor(object):
    def test_list(self):
        resp = Vendor.list(missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, list)
        assert isinstance(resp.data[0], Vendor)
        assert isinstance(resp.data[0].id, int)
