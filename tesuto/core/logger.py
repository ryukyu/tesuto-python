"""Logger.

Configure logging as appropriate for your project if you are exending this library.
"""
import os
import sys
import datetime
import logging.config

from tesuto.core.configs import config


class FileLogger(object):
    """FileLogger

    Custom logging to a file.
    """
    def __init__(self, _id: str = 'tesuto', path: str = '/tmp',
                 filename: str = None, stdout: bool = True,
                 log_format: str = "{id}:{datetime}:{level}: {message}"):
        self.id = _id
        self.stdout = stdout
        self.filename = filename
        self.path = path
        self.handler = self.configure(path, filename)
        self.log_format = log_format

    def configure(self, path: str, filename: str):
        logfile = os.path.join(path, str(filename) + '.log')
        if not os.path.exists(os.path.dirname(logfile)):
            os.makedirs(os.path.dirname(logfile))
        return open(logfile, 'a')

    def write(self, level: str = 'info', output: str = '', *args):
        if output:
            if len(args) > 0:
                output = output.format(*args)
        else:
            return

        if self.handler:
            self.handler.write(self.log_format.format(
                id=self.id,
                datetime=datetime.datetime.now(),
                level=level,
                message=output)
            )
            self.handler.flush()

        if self.stdout:
            sys.stdout.write(output)
            sys.stdout.flush()

    def debug(self, output: str, *args):
        self.write('debug', output, *args)

    def log(self, level: str = 'info',  output: str = '', *args):
        log = getattr(self, level)
        log(output, *args)


def configure_logging(log_handler: str = None, log_level: str = None,
                      log_file: str = None, debug: bool = None,
                      logger: str = ''):
    log_handler = log_handler or config.get('LOG_HANDLER')
    log_level = log_level or config.get('LOG_LEVEL')
    log_file = log_file or config.get('LOG_FILE')
    debug = debug or config.getboolean('DEBUG')

    if debug:
        log_level = 'DEBUG'

    log_conf = {
        'version': 1,
        'disable_exiting_loggers': False,
        'formatters': {
            'default': {
                'format': ('%(asctime)s %(name)s[%(process)s]:[%(levelname)s:'
                           '%(filename)s:%(funcName)s(%(lineno)d)] %(message)s'),
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            'default': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'default'
            },
            'null': {
                'level': 'DEBUG',
                'class': 'logging.NullHandler'
            },
        }
    }

    if log_handler == 'file':
        if not os.path.exists(os.path.dirname(log_file)):
            os.path.exists(os.path.dirname(log_file))

        log_conf['handlers']['file'] = {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filename': log_file
        }

    if logger:
        log_conf['loggers'] = {
            logger: {
                'handlers': [log_handler],
                'level': log_level.upper(),
                'propagate': True
            }
        }
    else:
        log_conf['loggers'] = {
            '': {
                'handlers': [log_handler],
                'level': log_level.upper(),
                'propagate': True
            }
        }

    logging.config.dictConfig(log_conf)
    logger = logging.getLogger(logger)
    return logger
