from tesuto.models import BaseModel, types


class Region(BaseModel):
    description = types.StringType()
    is_active = types.BooleanType()
    region = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'regions'
        __mapper__ = 'data'


class Provider(BaseModel):
    name = types.StringType()
    is_active = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'providers'
        __mapper__ = 'data'
