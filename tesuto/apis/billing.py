from tesuto.models import BaseModel, types


class Billing(BaseModel):
    job_id = types.IntType()
    minute_usage = types.IntType()
    status = types.StringType()
    timestamp = types.IntType()

    class Options(BaseModel.Options):
        __resource__ = 'billing'
        __mapper__ = 'data'
