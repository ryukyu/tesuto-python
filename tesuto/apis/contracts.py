from tesuto.models import BaseModel, types


class Subscription(BaseModel):
    status = types.StringType()
    minutes = types.IntType()
    name = types.StringType()
    description = types.StringType()
    specification = types.StringType()
    price = types.FloatType()

    class Options(BaseModel.Options):
        __resource__ = 'subscriptions'
        __mapper__ = 'data'


class Contract(BaseModel):
    organization_id = types.IntType()
    start_date = types.IntType()
    end_date = types.IntType()
    trial_end = types.IntType()
    status = types.StringType()
    contract_type = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'contracts'
        __mapper__ = 'data'

    @types.serializable
    def subscriptions(self):
        pass
